# PhoneGap exercise

![Screenshot](https://bytebucket.org/mikkohytonen/phonegap-exercise/raw/fda34c3d1852b6e56c23d7536ce83de3e021aebf/screenshot.jpg "Screenshot")
[Final app for Android](https://bitbucket.org/mikkohytonen/phonegap-exercise/raw/ea55d0259573bdbe17ef7a828221688a55d68485/android-debug.apk)

## Start PhoneGap GUI

1. Check if PhoneGap is installed on your computer
2. If not, install from http://phonegap.com/products/#desktop-app-section
3. Start it

## Create project

1. Click the +
2. Select Hello World template (do note other available templates)
3. Choose folder
4. Name your app
5. Give it an identifier, for example fi.jamk.G7966exercise
    * Should contain only alphanumeric characters to avoid cross-platform issues
    * Should be unique, important if submitting to app stores
    * Changing it afterwards can cause a mess
6. Done

## Look at config.xml

It contains many important settings http://docs.phonegap.com/phonegap-build/configuring/

* Information about app, like name
* Plugins for PhoneGap Build
* Links to different sizes for icons and splash screens
    * http://docs.phonegap.com/phonegap-build/configuring/icons-and-splash/
    * Different sizes can be generated, for example in http://pgicons.abiro.com/

## Install PhoneGap mobile app for testing

Can be installed from App Store, Google Play and Windows Store

* **Does not usually work with labranet network**, but during exercise you can connect your phone to Wi-Fi hotspot **PhoneGap Exercise**, password **cicada-eggshell**
* After installing open the app
* Give it the IP address from desktop app, and it should download the project to your mobile
* Desktop and mobile need to use same wlan, see first point

## Add plugins

This exercise uses following plugins, all bundled in PhoneGap app

* https://github.com/apache/cordova-plugin-device
* https://github.com/apache/cordova-plugin-network-information
* https://github.com/apache/cordova-plugin-camera
* https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin
* https://github.com/timkim/phonegap-plugin-barcodescanner
* https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-inappbrowser/

For now **installing plugins can be skipped**, but is needed if publishing an app without PhoneGap Build.

### For PhoneGap Build

Simply add plugins to config.xml. Most are included in project template.

* <plugin name="cordova-plugin-device" source="npm" spec="~1.1.1"/>
* <plugin name="cordova-plugin-network-information" source="npm" spec="~1.2.0"/>
* <plugin name="cordova-plugin-camera" source="npm" spec="~2.1.1"/>
* <plugin name="cordova-plugin-socialsharing" spec="https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin" />
* <plugin name="phonegap-plugin-barcodescanner" spec="https://github.com/timkim/phonegap-plugin-barcodescanner" />
* <plugin name="cordova-plugin-inappbrowser" source="npm" spec="~1.3.0"/>

### For manual build

See "Test manually" below.

## 1. Check some common events

Open www/js/index.js in an editor.

    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener("offline", this.networkStatus, false);
        document.addEventListener("online", this.networkStatus, false);
        document.addEventListener('pause', this.onPause, false);
        document.addEventListener('resume', this.onResume, false);
        document.addEventListener('backbutton', this.onBackButton, false);
    },

    onPause: function() {

        // You might want to save stuff, stop playing music, send analytics event

        app.receivedEvent('pause');
    },

    onResume: function() {

        // You might want to continue what you were doing or hide statusbar again
        alert('Resumed app');

        app.receivedEvent('resume');
    },

    onBackButton: function() {

        // On android, here you can control what happens from back button
        navigator.app.exitApp();

    },

    networkStatus: function () {

        if (!navigator.connection) {

            alert('Oh my, network information not available');

        } else {
            if (navigator.connection.type == Connection.NONE) {
                alert('Offline');
            } else {
                alert('Online');
            }
        }

    },

## 2. Check device os

index.js

    onDeviceReady: function() {
        app.receivedEvent('deviceready');

        if (window.device && device.platform) {
            document.querySelector('body').className += device.platform.toLowerCase();
        }

    },

## 3. Take a snapshot with camera

index.html

    <div class="controls">
      <button id="snap-take" onclick="void(0)">Take snap</button>
    </div>

index.js

    onDeviceReady: function() {
        app.receivedEvent('deviceready');

        if (window.device && device.platform) {
            document.querySelector('body').className += device.platform.toLowerCase();
        }

        document.querySelector('#snap-take').addEventListener('click', app.snapTake, false);

    },

    snapCurrent: null,

    snapTake: function () {

        if (!navigator.camera) {

            alert('Oh my, camera plugin missing');

        } else {

            var options = {
                quality: 70,
                allowEdit: false,
                correctOrientation: true,
                destinationType: Camera.DestinationType.FILE_URI
            };

            navigator.camera.getPicture(app.snapPreview, app.snapFail, options);

        }

    },

    snapPreview: function (uri) {

        app.snapCurrent = uri;
        document.querySelector('#snap-preview').src = uri;

    },

    snapFail: function (error) {

        alert('Oh snap: ' + (error.message || error));

    },

## 4. Share the snapshot

index.html

    <div class="controls">
      <button id="snap-take" onclick="void(0)">Take snap</button>
      <button id="snap-share" onclick="void(0)">Share snap</button>
    </div>

index.js

    onDeviceReady: function() {
        app.receivedEvent('deviceready');

        if (window.device && device.platform) {
            document.querySelector('body').className += device.platform.toLowerCase();
        }

        document.querySelector('#snap-take').addEventListener('click', app.snapTake, false);
        document.querySelector('#snap-share').addEventListener('click', app.snapShare, false);

    },

    snapShare: function () {

        if (!window.plugins || !window.plugins.socialsharing) {

            alert('Oh my, share plugin missing');

        } else if (!app.snapCurrent) {

            alert('Take a snap first');

        } else {

            var options = {
                subject: 'Look at this',
                message: 'Look at this',
                //url: url,
                files: [app.snapCurrent]
            };

            window.plugins.socialsharing.shareWithOptions(options);

        }
    },

## 5. Scan QR code, sample QR code below

index.html

    <div class="controls">
      <button id="snap-take" onclick="void(0)">Take snap</button>
      <button id="snap-share" onclick="void(0)">Share snap</button>
      <button id="qr-scan" onclick="void(0)">Scan QR code</button>
    </div>

index.js


    onDeviceReady: function() {
        app.receivedEvent('deviceready');

        if (window.device && device.platform) {
            document.querySelector('body').className += device.platform.toLowerCase();
        }

        document.querySelector('#snap-take').addEventListener('click', app.snapTake, false);
        document.querySelector('#snap-share').addEventListener('click', app.snapShare, false);
        document.querySelector('#qr-scan').addEventListener('click', app.qrScan, false);

    },

    qrScan: function () {

        if (!cordova.plugins || !cordova.plugins.barcodeScanner) {

            alert('Oh my, barcode scanner plugin missing');

        } else {

            var options = {
                "preferFrontCamera" : false, // iOS and Android
                "showFlipCameraButton" : false, // iOS and Android
                "prompt" : "Place a QR code inside the scan area", // supported on Android only
                "formats" : "QR_CODE,PDF_417" // default: all but PDF_417 and RSS_EXPANDED
            };

            cordova.plugins.barcodeScanner.scan(app.qrSuccess, app.qrFail, options);

        }
    },

    qrSuccess: function (result) {

        alert(result.text);

    },

    qrFail: function (error) {

        alert("Scan failed: " + (error.message || error));

    },

## 6. View the website from QR code

    qrSuccess: function (result) {

        if (confirm("View website?\n" + result.text)) {

            app.openUrl(result.text);

        }

    },

    openUrl: function (url) {

        if (!cordova.InAppBrowser) {

            app.alert('Oh my, barcode scanner plugin missing');

        } else {

            var ref = cordova.InAppBrowser.open(url, '_blank', 'location=yes');

        }

    }

## Test manually

First you need PhoneGap command-line tool, which can be installed with npm.
This can be impossible or complicated on school machines

* https://docs.npmjs.com/getting-started/installing-node
* npm install -g phonegap@latest

Add plugins using command-line:

* phonegap plugin add cordova-plugin-device
* phonegap plugin add cordova-plugin-network-information
* phonegap plugin add cordova-plugin-camera
* phonegap plugin add cordova-plugin-x-socialsharing
* phonegap plugin add phonegap-plugin-barcodescanner
* phonegap plugin add phonegap-plugin-inappbrowser

Add platforms using command-line:

* phonegap platform add android
* phonegap platform add ios

This should result in native source code in /platforms folder

After changes source codes can be updated with

* phonegap prepare android
* phonegap prepare ios

You can open native source code in Android Studio or xCode

## Test material

### QR Code

![QR code](https://bytebucket.org/mikkohytonen/phonegap-exercise/raw/b720f8d43351d7550ee002f2bf2b73c5cc97ff28/sample_qr_code.jpg "QR code")

### Augmented reality demo (doesn't start on iOS 10, go figure)

* Input ar.phonegap.com as Server Address in PhoneGap App
  * ![Like so](http://docs.phonegap.com/images/dev-app-wikitude.jpg "Like so")
* And point the demo to the target image below
  * ![AR target image](http://phonegap.com/blog/uploads/2016-03/samurai_swag.png)
* [AR tutorial](http://docs.phonegap.com/tutorials/develop/wikitude/)

### Push notifications

http://docs.phonegap.com/tutorials/develop/push-notifications/
