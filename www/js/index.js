/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener("offline", this.networkStatus, false);
        document.addEventListener("online", this.networkStatus, false);
        document.addEventListener('pause', this.onPause, false);
        document.addEventListener('resume', this.onResume, false);
        document.addEventListener('backbutton', this.onBackButton, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');

        if (window.device && device.platform) {
            document.querySelector('body').className += device.platform.toLowerCase();
        }

        document.querySelector('#snap-take').addEventListener('click', app.snapTake, false);
        document.querySelector('#snap-share').addEventListener('click', app.snapShare, false);
        document.querySelector('#qr-scan').addEventListener('click', app.qrScan, false);

        app.networkStatus();

    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },

    alert: function (message) {

        if (navigator.notification) {
            navigator.notification.alert(message);
        } else {
            window.alert(message);
        }

    },

    confirm: function (message, callback, title) {

        if (navigator.notification) {
            navigator.notification.confirm(message, callback, title);
        } else {
            if (window.confirm(title + "\n" + message)) {
                callback(1);
            }
        }

    },

    onPause: function() {

        // You might want to save stuff, stop playing music, send analytics event

        app.receivedEvent('pause');
    },

    onResume: function() {

        // You might want to continue what you were doing or hide statusbar again
        app.alert('Resumed app');

        app.receivedEvent('resume');
    },

    onBackButton: function() {

        // On android, here you can control what happens from back button
        navigator.app.exitApp();

    },

    networkStatus: function () {

        if (!navigator.connection) {

            app.alert('Oh my, network information not available');

        } else {
            if (navigator.connection.type == Connection.NONE) {
                app.alert(device.platform + ' ' + device.version + ' offline');
            } else {
                app.alert(device.platform + ' ' + device.version + ' online');
            }
        }

    },

    snapCurrent: null,

    snapTake: function () {

        if (!navigator.camera) {

            app.alert('Oh my, camera plugin missing');

        } else {

            var options = {
                quality: 70,
                allowEdit: false,
                correctOrientation: true,
                destinationType: Camera.DestinationType.FILE_URI
            };

            navigator.camera.getPicture(app.snapPreview, app.snapFail, options);

        }

    },

    snapPreview: function (uri) {

        app.snapCurrent = uri;
        document.querySelector('#snap-preview').src = uri;

    },

    snapFail: function (error) {

        app.alert('Oh snap: ' + (error.message || error));

    },

    snapShare: function () {

        if (!window.plugins || !window.plugins.socialsharing) {

            app.alert('Oh my, share plugin missing');

        } else if (!app.snapCurrent) {

            app.alert('Take a snap first');

        } else {

            var options = {
                subject: 'Look at this',
                message: 'Look at this',
                //url: url,
                files: [app.snapCurrent]
            };

            window.plugins.socialsharing.shareWithOptions(options);

        }
    },

    qrScan: function () {

        if (!cordova.plugins || !cordova.plugins.barcodeScanner) {

            app.alert('Oh my, barcode scanner plugin missing');

        } else {

            var options = {
                "preferFrontCamera" : false, // iOS and Android
                "showFlipCameraButton" : false, // iOS and Android
                "prompt" : "Place a QR code inside the scan area", // supported on Android only
                "formats" : "QR_CODE,PDF_417" // default: all but PDF_417 and RSS_EXPANDED
            };

            cordova.plugins.barcodeScanner.scan(app.qrSuccess, app.qrFail, options);

        }
    },

    qrSuccess: function (result) {

        var callback = function (buttonIndex) {

            buttonIndex = buttonIndex || 1;
            if (buttonIndex == 1) {
                app.openUrl(result.text);
            }

        };

        app.confirm(result.text, callback, 'View website?');

    },

    qrFail: function (error) {

        app.alert("Scan failed: " + (error.message || error));

    },

    openUrl: function (url) {

        if (!cordova.InAppBrowser) {

            app.alert('Oh my, barcode scanner plugin missing');

        } else {

            var ref = cordova.InAppBrowser.open(url, '_blank', 'location=yes');

        }

    }

};
